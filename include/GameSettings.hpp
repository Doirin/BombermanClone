#pragma once

#include <string>

namespace Settings
{
    static const std::string WINDOW_TITLE = "Bomberman Clone";
    static const unsigned int WINDOW_WIDTH = 1240;
    static const unsigned int WINDOW_HEIGHT = 680;
    static const unsigned int TILE_SIZE = 40;

    static const std::string LOG_PATH = "log/log.txt";
    
    static const bool PRINT_DEBUG_MESSAGES = true;
}