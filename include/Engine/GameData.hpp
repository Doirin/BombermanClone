#pragma once

#include <memory>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>

namespace Engine
{
    class ResourceManager;
    class SceneManager;

    struct GameData
    {
        bool gameRunning;
        SDL_Window *window;
        SDL_Renderer *renderer;
        std::unique_ptr<ResourceManager> resourceManager;
        std::unique_ptr<SceneManager> sceneManager;
    };

    typedef std::shared_ptr<GameData> GameDataRef;
}