#pragma once

#include "GameData.hpp"

#include <string>
#include <map>

#include <SDL2/SDL.h>

namespace Engine
{
    class ResourceManager
    {
    public:
        ResourceManager(GameDataRef gameData);

        void loadPNG(const std::string &path, const std::string &resourceName);
        SDL_Texture* getTexture(const std::string &resourceName);

    private:
        GameDataRef gameData;
        std::map<std::string, SDL_Texture*> images;
    };
}