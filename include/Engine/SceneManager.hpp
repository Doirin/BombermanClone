#pragma once

#include "Scene.hpp"
#include "GameData.hpp"

#include <stack>
#include <memory>

namespace Engine
{
    typedef std::unique_ptr<Scene> SceneRef;

    class SceneManager
    {
    public:
        SceneManager(GameDataRef gameData);
        virtual ~SceneManager();

        void addScene(SceneRef scene, const bool replace = true, const bool replaceAll = false);
        void removeScene();
        void process();

        const int getScenesCount();
        SceneRef &getActiveScene();
    
    private:
        GameDataRef gameData_;
        SceneRef scene_;

        bool replace_;
        bool replaceAll_;
        bool add_;
        bool remove_;

        std::stack<SceneRef> scenes_;
    };
}