#pragma once

#include <random>
#include <chrono>

namespace Engine
{
    class Randomizer
    {
    public:
        Randomizer();

        const double getRandomRange(double min, double max);

    private:
        std::mt19937 mt_;
    };
}