#pragma once

#include <string>

namespace Engine
{
    class Logger
    {
    public:
        void static log(const std::string &message);
        void static showErrorBox(const std::string &message);
        bool static writeToFile(const std::string &message);
    };
}