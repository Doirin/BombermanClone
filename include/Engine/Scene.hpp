#pragma once

#include <SDL2/SDL_rect.h>

namespace Engine
{
    class Scene
    {
    public:
        virtual ~Scene() {};

        virtual void handleInput() = 0;
        virtual void update(double deltaTime, bool updatePrevious = false);
        virtual void render();

        void setPreviousScene(Scene *scene);

    protected:
        Scene *previousScene_ = nullptr;

        void renderPreviousScene();
        void updatePreviousScene(double deltaTime);
    };
}