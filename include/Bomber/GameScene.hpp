#pragma once

#include "Engine/Scene.hpp"
#include "Engine/GameData.hpp"

#include "Bomber/TileMap.hpp"
#include "Bomber/TileType.hpp"
#include "Bomber/Tile.hpp"
#include "Bomber/Actors/Actor.hpp"

#include <map>
#include <vector>

namespace Bomber
{

    class GameScene : public Engine::Scene
    {
    public:
        GameScene(Engine::GameDataRef gameData);
        GameScene(Engine::GameDataRef gameData, SDL_Rect &sceneRect);
        virtual ~GameScene();

        virtual void handleInput();
        virtual void update(double deltaTime, bool updatePrevious = false);
        virtual void render();

    private:
        void init();

        void movePlayerActor(Direction direction, const unsigned int step = 1);
        void setActorPosition(Actor *actor, SDL_FRect &destRect);

        double baseSpeed;

        SDL_Rect sceneRect_ = {0, 0, 0, 0};
        SDL_Rect pauseSceneRect_ = {200, 200, 500, 500};

        Engine::GameDataRef gameData_;

        Actor *playerActor_;
        SDL_FRect playerDest_;

        std::vector<Tile> tiles_;
        std::vector<Actor*> actors_;
        std::map<TileType, SDL_Texture*> tileTexture_;

        std::shared_ptr<TileMap> tileMap_;
    };
}