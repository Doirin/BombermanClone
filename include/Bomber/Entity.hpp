#pragma once

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>

namespace Bomber
{
    class Entity
    {
    public:
        Entity() {};
        virtual ~Entity() {};

        void setPosition(const float posX, const float posY);
        SDL_FRect getRect();
        
        void setTexture(SDL_Texture *texture);
        SDL_Texture* getTexture();

    protected:
        SDL_FRect rect_;
        SDL_Texture* texture_;
    };
}
