#pragma once

#include "Engine/Scene.hpp"
#include "Engine/GameData.hpp"

#include <memory>

#include <SDL2/SDL.h>

namespace Bomber
{
    class MainScene : public Engine::Scene
    {
    public:
        MainScene(Engine::GameDataRef gameData);
        MainScene(Engine::GameDataRef gameData, SDL_Rect &sceneRect);
        virtual ~MainScene();

        virtual void handleInput();
        virtual void update(double deltaTime, bool updatePrevious = false);
        virtual void render();

    private:
        SDL_Rect sceneRect_ = {0, 0, 0, 0};
        SDL_Rect gameSceneRect_ = {100, 100, 900, 0};
    
        Engine::GameDataRef gameData_;
    };
}