#pragma once

enum class TileType
{
    Wall,
    Grass,
    Box
};
