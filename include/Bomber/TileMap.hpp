#pragma once

#include "Tile.hpp"
#include "Entity.hpp"
#include "Engine/GameData.hpp"

#include <vector>

namespace Bomber
{
    class TileMap
    {
    public:
        TileMap(const unsigned int mapWidth, const unsigned int mapHeight);
        ~TileMap();

        std::vector<TileType> getTileMap();
        TileType getTileTypeAtPosition(const unsigned int posX, const unsigned int posY);

    private:
        void init();

        const unsigned int mapWidth_, mapHeight_;

        std::vector<TileType> tileMap_;
        std::vector<Entity*> entities_;
    };
}
