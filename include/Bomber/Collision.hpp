#pragma once

#include <SDL2/SDL_rect.h>

namespace Bomber
{
    class Entity;
    class TileMap;

    class Collision
    {
    public:
        static bool checkRectCollision(SDL_FRect rect, TileMap *map);
    };
}
