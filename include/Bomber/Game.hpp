#pragma once

#include "Engine/Scene.hpp"
#include "Engine/SceneManager.hpp"
#include "Engine/ResourceManager.hpp"
#include "Engine/GameData.hpp"

#include <memory>
#include <string>

#include <SDL2/SDL.h>

namespace Bomber
{
    class Game
    {
    public:
        Game(const unsigned int windowWidth, const unsigned int windowHeight, const std::string &windowTitle);
        virtual ~Game();

        void run();
    
    private:
        void init();

        const unsigned int windowWidth;
        const unsigned int windowHeight;
        const std::string windowTitle;

        SDL_Window *window = nullptr;
        SDL_Surface *screenSurface = nullptr;
        SDL_Renderer *renderer = nullptr;

        Engine::GameDataRef gameData = std::make_shared<Engine::GameData>();
    };
}
