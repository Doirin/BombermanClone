#pragma once

namespace Bomber
{
    class Action
    {
    public:
        Action();
        virtual ~Action() {};

        virtual bool execute() = 0;
    };
}