#pragma once

#include "Bomber/Actors/Actor.hpp"

namespace Bomber
{
    class Bomberman : public Actor
    {
    public:
        Bomberman(const float posX, const float posY);
        virtual ~Bomberman();

    private:

    };
}
