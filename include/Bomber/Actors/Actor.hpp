#pragma once

#include "Bomber/Entity.hpp"

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>

namespace Bomber
{
    class Camera;

    enum class Direction
    {
        None = 0,
        Up,
        Right,
        Down,
        Left
    };

    enum class State
    {
        None = 0,
        Move,
        Place
    };

    class Actor : public Entity
    {
    public:
        Actor();
        virtual ~Actor() {};

        void setCamera(Camera &camera);
        bool move();

        void setState(State state);
        const State getState();

        void setDirection(Direction direction);
        const Direction getDirection();

    protected:
        SDL_Texture* texture_ = nullptr;

        State currentState_;
        Direction currentDirection_;
    };
}
