#pragma once

#include "Bomber/Entity.hpp"

#include <SDL2/SDL_rect.h>

class Actor;

namespace Bomber
{
    class Bomb : public Entity
    {
    public:
        Bomb(SDL_FRect rect);


        SDL_FRect getRect();
    
    private:
    };
}
