#pragma once

#include <SDL2/SDL_rect.h>

class Actor;

namespace Bomber
{
    class Camera
    {
    public:
        Camera(SDL_FRect rect);

        void followActor(Actor &actor);
        void followCoords(const float x, const float y);

        SDL_FRect getRect();
    
    private: 
        SDL_FRect cameraRect_;

        float camPosX_, camPosY_;
    };
}
