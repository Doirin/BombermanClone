#pragma once

#include "Engine/Scene.hpp"
#include "Engine/GameData.hpp"

namespace Bomber
{
    class PauseScene : public Engine::Scene
    {
    public:
        PauseScene(Engine::GameDataRef gameData);
        PauseScene(Engine::GameDataRef gameData, SDL_Rect &sceneRect);
        virtual ~PauseScene();

        virtual void handleInput();
        virtual void update(double deltaTime, bool updatePrevious = false);
        virtual void render();

    private:
        SDL_Rect sceneRect_ = {0, 0, 0, 0};

        Engine::GameDataRef gameData_;
    };
}