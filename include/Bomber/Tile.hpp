#pragma once

#include "Bomber/Entity.hpp"

#include <SDL2/SDL_rect.h>

class Camera;
class SDL_Texture;
class SDL_Renderer;
enum class TileType;

namespace Bomber
{
    class Tile : public Entity
    {
    public:
        Tile(const float posX, const float posY, TileType tileType);

        SDL_Texture *texture;
        SDL_FRect box;
        TileType tileType;
    protected:
        SDL_FRect rect_;
        SDL_Texture* texture_ = nullptr;
    };
}
