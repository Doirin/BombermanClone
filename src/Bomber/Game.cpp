#include "Bomber/Game.hpp"
#include "Bomber/MainScene.hpp"

#include "Engine/Logger.hpp"
#include "Engine/ResourceManager.hpp"
#include "Engine/SceneManager.hpp"
#include "Engine/GameData.hpp"

#include <exception>
#include <memory>
#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

namespace Bomber
{
    Game::Game(const unsigned int windowWidth, const unsigned int windowHeight, const std::string &windowTitle)
        : windowWidth(windowWidth), windowHeight(windowHeight), windowTitle(windowTitle)
    {
        // Initialize SDL
        init();

        gameData->gameRunning = true;
        gameData->window = window;
        gameData->renderer = renderer;
        gameData->sceneManager = std::make_unique<Engine::SceneManager>(gameData);
        gameData->resourceManager = std::make_unique<Engine::ResourceManager>(gameData);
        
        gameData->sceneManager->addScene(std::make_unique<MainScene>(gameData));
        // gameData->resourceManager->loadPNG("resources/background_inner.png", "bginner");
    }

    Game::~Game()
    {
        SDL_DestroyRenderer(renderer);
        SDL_FreeSurface(screenSurface);
        SDL_DestroyWindow(window);

        Engine::Logger::log("SDL IMG quit");
        IMG_Quit();

        Engine::Logger::log("SDL quit");
        SDL_Quit();
    }

    void Game::run()
    {
        double deltaTime = 0.0005f;

        // Starting Game Loop
        while (gameData->gameRunning)
        {
            gameData->sceneManager->process();

            if (gameData->gameRunning)
            {
                gameData->sceneManager->getActiveScene()->handleInput();
                gameData->sceneManager->getActiveScene()->update(deltaTime);
                gameData->sceneManager->getActiveScene()->render();
            }
            
            SDL_RenderPresent(gameData->renderer);
            SDL_Delay(16);
        }

        Engine::Logger::log("Bye");
    }

    void Game::init()
    {
        Engine::Logger::log("SDL init");
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            Engine::Logger::showErrorBox("SDL init failed. Error: " + std::string(SDL_GetError()));
            throw;
        }
        
        Engine::Logger::log("SDL create window");
        window = SDL_CreateWindow(windowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if (window == nullptr)
        {
            Engine::Logger::showErrorBox("SDL create window failed. Error: " + std::string(SDL_GetError()));
            throw;
        }

        Engine::Logger::log("SDL create renderer");
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        screenSurface = SDL_GetWindowSurface(window);
    }
}