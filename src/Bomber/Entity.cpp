#include "Bomber/Entity.hpp"

namespace Bomber
{
    void Entity::setPosition(const float posX, const float posY)
    {
        rect_.x = posX;
        rect_.y = posY;
    }

    SDL_FRect Entity::getRect()
    {
        return rect_;
    }

    void Entity::setTexture(SDL_Texture *texture)
    {
        texture_ = texture;
    }

    SDL_Texture* Entity::getTexture()
    {
        return texture_;
    }
}