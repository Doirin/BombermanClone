#include "Bomber/GameScene.hpp"
#include "Bomber/Collision.hpp"
#include "Bomber/MainScene.hpp"
#include "Bomber/PauseScene.hpp"
#include "Bomber/TileMap.hpp"
#include "Bomber/Tile.hpp"
#include "Bomber/Actors/Actor.hpp"
#include "Bomber/Actors/Bomberman.hpp"
#include "GameSettings.hpp"

#include "Engine/Logger.hpp"
#include "Engine/SceneManager.hpp"
#include "Engine/ResourceManager.hpp"

#include <SDL2/SDL_events.h>

namespace Bomber
{
    GameScene::GameScene(Engine::GameDataRef gameData)
        : gameData_(gameData)
    {
        sceneRect_.x = 0;
        sceneRect_.y = 0;
        SDL_GetWindowSize(gameData_->window, &sceneRect_.w, &sceneRect_.h);
        Engine::Logger::log("GameScene: 1 param constructor");
        init();
    }

    GameScene::GameScene(Engine::GameDataRef gameData, SDL_Rect &sceneRect)
        : sceneRect_(sceneRect), gameData_(gameData)
    {
        if (sceneRect_.w == 0 || sceneRect_.h == 0)
        {
            sceneRect_.x = 0;
            sceneRect_.y = 0;
            SDL_GetWindowSize(gameData_->window, &sceneRect_.w, &sceneRect_.h);
        }
        Engine::Logger::log("GameScene: 2 params constructor");
        init();
    }

    GameScene::~GameScene()
    {
        Engine::Logger::log("GameScene: destructor");
        delete playerActor_;
    }

    void GameScene::handleInput()
    {
        // TODO create an input manager
        SDL_Event event;
        while (SDL_PollEvent(&event) != 0)
        {
            if (event.type == SDL_QUIT)
            {
                gameData_->gameRunning = false;
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    gameData_->sceneManager->addScene(std::make_unique<PauseScene>(gameData_, pauseSceneRect_), false);
                    break;
                case SDLK_UP:
                    movePlayerActor(Direction::Up);
                    break;
                case SDLK_RIGHT:
                    movePlayerActor(Direction::Right);
                    break;
                case SDLK_DOWN:
                    movePlayerActor(Direction::Down);
                    break;
                case SDLK_LEFT:
                    movePlayerActor(Direction::Left);
                    break;
                default:
                    break;
                }
            }
        }
    }

    void GameScene::update(double deltaTime, bool updatePrevious)
    {
        Scene::update(deltaTime, updatePrevious);
        
        if (playerActor_->getState() == State::Move)
        {
            SDL_FRect playerRect = playerActor_->getRect();

            switch (playerActor_->getDirection())
            {
            case Direction::Up:
                playerActor_->setPosition(playerRect.x, playerRect.y -= deltaTime * baseSpeed);
                if (playerRect.y < playerDest_.y)
                {
                    setActorPosition(playerActor_, playerDest_);
                }
                break;
            case Direction::Right:
                playerActor_->setPosition(playerRect.x += deltaTime * baseSpeed, playerRect.y);
                if (playerRect.x > playerDest_.x)
                {
                    setActorPosition(playerActor_, playerDest_);
                }
                break;
            case Direction::Down:
                playerActor_->setPosition(playerRect.x, playerRect.y += deltaTime * baseSpeed);
                if (playerRect.y > playerDest_.y)
                {
                    setActorPosition(playerActor_, playerDest_);
                }
                break;
            case Direction::Left:
                playerActor_->setPosition(playerRect.x -= deltaTime * baseSpeed, playerRect.y);
                if (playerRect.x < playerDest_.x)
                {
                    setActorPosition(playerActor_, playerDest_);
                }
                break;
            
            default:
                break;
            }
        }
    }

    void GameScene::render()
    {
        Scene::render();

        // TODO restrict rendering to camera view
        for (size_t i = 0; i < tiles_.size(); i++)
        {
            SDL_Rect tileRect;
            tileRect.x = (tiles_.at(i).box.x * tiles_.at(i).box.w) + sceneRect_.x;
            tileRect.y = (tiles_.at(i).box.y * tiles_.at(i).box.h) + sceneRect_.y;
            tileRect.w = tiles_.at(i).box.w;
            tileRect.h = tiles_.at(i).box.h;
            SDL_RenderCopy(gameData_->renderer, tiles_.at(i).texture, NULL, &tileRect);
        }
        
        for (size_t i = 0; i < actors_.size(); i++)
        {
            SDL_Rect actorRect;
            actorRect.x = (actors_.at(i)->getRect().x * actors_.at(i)->getRect().w) + sceneRect_.x;
            actorRect.y = (actors_.at(i)->getRect().y * actors_.at(i)->getRect().h) + sceneRect_.y;
            actorRect.w = actors_.at(i)->getRect().w;
            actorRect.h = actors_.at(i)->getRect().h;

            SDL_RenderCopy(gameData_->renderer, actors_.at(i)->getTexture(), NULL, &actorRect);
        }
    }

    void GameScene::init()
    {
        gameData_->resourceManager->loadPNG("resources/Tile/Box.png", "tileBox");
        gameData_->resourceManager->loadPNG("resources/Tile/Wall.png", "tileWall");
        gameData_->resourceManager->loadPNG("resources/Tile/Grass.png", "tileGrass");
        gameData_->resourceManager->loadPNG("resources/Tile/Circle.png", "bomber");
        gameData_->resourceManager->loadPNG("resources/Tile/Select.png", "select");

        baseSpeed = 100.f;

        unsigned int tileMapWidth = sceneRect_.w / Settings::TILE_SIZE;
        unsigned int tileMapHeight = sceneRect_.h / Settings::TILE_SIZE;
        tileMap_ = std::make_shared<TileMap>(tileMapWidth, tileMapHeight);

        tileTexture_.insert(std::make_pair(TileType::Wall, gameData_->resourceManager->getTexture("tileWall")));
        tileTexture_.insert(std::make_pair(TileType::Grass, gameData_->resourceManager->getTexture("tileGrass")));
        tileTexture_.insert(std::make_pair(TileType::Box, gameData_->resourceManager->getTexture("tileBox")));

        // TODO move this to a more approriate place, i.e. TileMap
        unsigned int posX = 0;
        unsigned int posY = 0;
        for (size_t i = 0; i < tileMap_->getTileMap().size(); i++, posX++)
        {
            if (posX == tileMapWidth)
            {
                posX = 0;
                posY++;
            }

            Tile tile(posX, posY, tileMap_->getTileMap()[i]);
            tile.texture = tileTexture_[tileMap_->getTileMap()[i]];

            tiles_.push_back(tile);
        }

        playerActor_ = new Bomberman(1, 1);
        playerActor_->setTexture(gameData_->resourceManager->getTexture("bomber"));
        actors_.push_back(playerActor_);

        Actor *actor1 = new Bomberman(11, 8);
        actor1->setTexture(gameData_->resourceManager->getTexture("bomber"));
        actors_.push_back(actor1);

        Actor *actor2 = new Bomberman(12, 13);
        actor2->setTexture(gameData_->resourceManager->getTexture("bomber"));
        actors_.push_back(actor2);

    }

    void GameScene::setActorPosition(Actor *actor, SDL_FRect &destRect)
    {
        actor->setPosition(destRect.x, destRect.y);
        actor->setState(State::None);
        actor->setDirection(Direction::None);
    }

    void GameScene::movePlayerActor(Direction direction, const unsigned int step)
    {
        if (playerActor_->getState() == State::None)
        {
            bool changed = false;
            playerDest_ = playerActor_->getRect();
            SDL_FRect updatedRect = playerDest_;
            playerActor_->setState(State::Move);

            switch (direction)
            {
            case Direction::Up:
                playerActor_->setDirection(Direction::Up);
                updatedRect.y--;
                changed = true;
                break;
            case Direction::Right:
                playerActor_->setDirection(Direction::Right);
                updatedRect.x++;
                changed = true;
                break;
            case Direction::Down:
                playerActor_->setDirection(Direction::Down);
                updatedRect.y++;
                changed = true;
                break;
            case Direction::Left:
                playerActor_->setDirection(Direction::Left);
                updatedRect.x--;
                changed = true;
                break;
            default:
                break;
            }

            if (changed == true && Collision::checkRectCollision(updatedRect, tileMap_.get()))
            {
                playerDest_ = updatedRect;
            }
        }
    }
}