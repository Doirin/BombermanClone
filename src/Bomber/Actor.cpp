#include "Bomber/Actors/Actor.hpp"

namespace Bomber
{
    Actor::Actor()
    {
        currentState_ = State::None;
        currentDirection_ = Direction::None;
    }

    bool Actor::move()
    {
        switch (currentDirection_)
        {
        case Direction::Up:
            rect_.y--;
            break;
        case Direction::Right:
            rect_.x++;
            break;
        case Direction::Down:
            rect_.y++;
            break;
        case Direction::Left:
            rect_.x--;
            break;
        default:
            break;
        }

        return true;
    }

    void Actor::setState(State state)
    {
        currentState_ = state;
    }

    const State Actor::getState()
    {
        return currentState_;
    }

    void Actor::setDirection(Direction direction)
    {
        currentDirection_ = direction;
    }

    const Direction Actor::getDirection()
    {
        return currentDirection_;
    }
}