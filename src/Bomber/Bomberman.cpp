#include "Bomber/Actors/Bomberman.hpp"
#include "Bomber/Actors/Actor.hpp"

#include "GameSettings.hpp"

namespace Bomber
{
    Bomberman::Bomberman(const float posX, const float posY)
    {
        rect_.x = posX;
        rect_.y = posY;
        rect_.w = Settings::TILE_SIZE;
        rect_.h = Settings::TILE_SIZE;
    }

    Bomberman::~Bomberman()
    {
    }
}