#include "Bomber/Collision.hpp"
#include "Bomber/TileMap.hpp"
#include "Bomber/TileType.hpp"
#include "Bomber/Entity.hpp"

#include <cstdio>

#include <vector>

namespace Bomber
{
    bool Collision::checkRectCollision(SDL_FRect rect, TileMap *map)
    {
        TileType tileType = map->getTileTypeAtPosition(rect.x, rect.y);
        return (tileType == TileType::Grass ? true : false);
    }
}