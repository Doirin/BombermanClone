#include "Bomber/Tile.hpp"
#include "Bomber/TileType.hpp"
#include "GameSettings.hpp"

#include <SDL2/SDL_render.h>

namespace Bomber
{
    Tile::Tile(const float posX, const float posY, TileType tileType)
    {
        unsigned int width, height;
        width = height = Settings::TILE_SIZE;
        box.x = posX;
        box.y = posY;
        box.w = width;
        box.h = height;

        this->tileType = tileType;
    }
}
