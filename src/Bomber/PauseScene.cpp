#include "Bomber/PauseScene.hpp"
#include "Bomber/MainScene.hpp"

#include "Engine/Logger.hpp"
#include "Engine/SceneManager.hpp"
#include "Engine/ResourceManager.hpp"

#include <SDL2/SDL_events.h>

namespace Bomber
{
    PauseScene::PauseScene(Engine::GameDataRef gameData)
        : gameData_(gameData)
    {
        Engine::Logger::log("PauseScene: 1 param constructor");
    }

    PauseScene::PauseScene(Engine::GameDataRef gameData, SDL_Rect &sceneRect)
        : sceneRect_(sceneRect), gameData_(gameData)
    {
        Engine::Logger::log("PauseScene: 2 params constructor");
    }

    PauseScene::~PauseScene()
    {
        Engine::Logger::log("PauseScene: destructor");
    }

    void PauseScene::handleInput()
    {
        // TODO create an input manager
        SDL_Event event;
        while (SDL_PollEvent(&event) != 0)
        {
           if (event.type == SDL_QUIT)
            {
                gameData_->gameRunning = false;
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    gameData_->sceneManager->removeScene();
                    break;
                case SDLK_q:
                    Engine::Logger::log("To MainScene");
                    gameData_->sceneManager->addScene(std::make_unique<MainScene>(gameData_), true, true);
                default:
                    break;
                }
            }
        }
    }

    void PauseScene::update(double deltaTime, bool updatePrevious)
    {
        Scene::update(deltaTime, updatePrevious);
    }

    void PauseScene::render()
    {
        Scene::render();
        SDL_SetRenderDrawColor(gameData_->renderer, 0xFF, 0x00, 0xFF, 0x00);
        SDL_RenderFillRect(gameData_->renderer, &sceneRect_);
    }
}