#include "Bomber/MainScene.hpp"
#include "Bomber/GameScene.hpp"

#include "Engine/Scene.hpp"
#include "Engine/Logger.hpp"
#include "Engine/GameData.hpp"
#include "Engine/ResourceManager.hpp"
#include "Engine/SceneManager.hpp"

#include <SDL2/SDL.h>

namespace Bomber
{
    MainScene::MainScene(Engine::GameDataRef gameData)
        : gameData_(gameData)
    {
        Engine::Logger::log("MainScene: 1 param constructor");
        // gameData->resourceManager->loadPNG("resources/background.png", "background");
        // background = gameData->resourceManager->getImage("background");
    }

    MainScene::MainScene(Engine::GameDataRef gameData, SDL_Rect &sceneRect)
        : sceneRect_(sceneRect), gameData_(gameData)
    {
        Engine::Logger::log("MainScene: 2 params constructor");
    }

    MainScene::~MainScene()
    {
        Engine::Logger::log("MainScene: destructor");
    }

    void MainScene::handleInput()
    {
        // TODO create an input manager
        SDL_Event event;
        while (SDL_PollEvent(&event) != 0)
        {
            if (event.type == SDL_QUIT)
            {
                gameData_->gameRunning = false;
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    gameData_->sceneManager->removeScene();
                    break;
                case SDLK_t:
                    gameData_->sceneManager->addScene(std::make_unique<GameScene>(gameData_));
                    break;
                default:
                    break;
                }
            }
        }
    }

    void MainScene::update(double deltaTime, bool updatePrevious)
    {
        Scene::update(deltaTime, updatePrevious);
    }

    void MainScene::render()
    {
        Scene::render();
        
        SDL_SetRenderDrawColor(gameData_->renderer, 0xFF, 0xFF, 0xFF, 0x00);
        SDL_RenderClear(gameData_->renderer);
    }
}