#include "Bomber/TileMap.hpp"
#include "Bomber/Tile.hpp"
#include "Bomber/TileType.hpp"

#include "Engine/GameData.hpp"

namespace Bomber
{
    TileMap::TileMap(const unsigned int mapWidth, const unsigned int mapHeight)
        : mapWidth_(mapWidth), mapHeight_(mapHeight)
    {
        init();
    }

    TileMap::~TileMap()
    {
    }

    void TileMap::init()
    {
        for (size_t y = 1; y <= mapHeight_; y++)
        {
            for (size_t x = 1; x <= mapWidth_; x++)
            {
                if (x == 1 || x == mapWidth_ || y == 1 || y == mapHeight_ ||
                        (x % 2 == 1 && y % 2 == 1))
                {
                    tileMap_.push_back(TileType::Wall);
                }
                else
                {
                    tileMap_.push_back(TileType::Grass);
                }
            }
        }
    }

    std::vector<TileType> TileMap::getTileMap()
    {
        return tileMap_;
    }

    TileType TileMap::getTileTypeAtPosition(const unsigned int posX, const unsigned int posY)
    {
        return tileMap_[posX + posY * mapWidth_];
    }
}