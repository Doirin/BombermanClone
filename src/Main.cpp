#include "Bomber/Game.hpp"
#include "GameSettings.hpp"

#ifndef MAIN
    #ifdef _WIN32
        #define MAIN WinMain
    #else
        #define MAIN main
    #endif
#endif

int MAIN(int argc, char *argv[])
{
    Bomber::Game game(Settings::WINDOW_WIDTH, Settings::WINDOW_HEIGHT, Settings::WINDOW_TITLE);
    game.run();
    return EXIT_SUCCESS;
}