#include "Engine/Randomizer.hpp"


namespace Engine
{
    Randomizer::Randomizer()
        : mt_{static_cast<std::uint32_t>(
            std::chrono::high_resolution_clock::now().time_since_epoch().count()
        )}
    {
    }

    const double Randomizer::getRandomRange(double min, double max)
    {
        std::uniform_real_distribution<double> distrib(min, max);
        return distrib(mt_);
    }
}