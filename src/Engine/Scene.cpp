#include "Engine/Scene.hpp"

namespace Engine
{
    void Scene::render()
    {
        renderPreviousScene();
    }

    void Scene::update(double deltaTime, bool updatePrevious)
    {
        if (updatePrevious)
        {
            updatePreviousScene(deltaTime);
        }
    }

    void Scene::setPreviousScene(Scene *scene)
    {
        previousScene_ = scene;
    }

    void Scene::renderPreviousScene()
    {
        if (previousScene_ != nullptr)
        {
            previousScene_->render();
        }
    }

    void Scene::updatePreviousScene(double deltaTime)
    {
        if (previousScene_ != nullptr)
        {
            previousScene_->update(deltaTime);
        }
    }
}