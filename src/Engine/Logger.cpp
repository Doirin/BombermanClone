#include "Engine/Logger.hpp"

#include "GameSettings.hpp"

#include <string>
#include <fstream>

#include <SDL2/SDL_messagebox.h>

namespace Engine
{
    void Logger::log(const std::string &message)
    {
        if (Settings::PRINT_DEBUG_MESSAGES)
        {
            printf((message + "\n").c_str());
        }
        
        Logger::writeToFile(message.c_str());
    }

    void Logger::showErrorBox(const std::string &messsage)
    {
        log(messsage.c_str());
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", messsage.c_str(), NULL);
    }

    bool Logger::writeToFile(const std::string &message)
    {
        std::ofstream file;

        file.open(Settings::LOG_PATH, std::ios_base::out | std::ios_base::app);
 
        if (file.fail())
        {
            return false;
        }

        file << message << "\n";

        file.close();
        return true;
    }
}