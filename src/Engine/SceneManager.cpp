#include "Engine/SceneManager.hpp"
#include "Engine/Scene.hpp"
#include "Engine/Logger.hpp"

#include <stack>

#include <SDL2/SDL.h>

namespace Engine
{
    SceneManager::SceneManager(GameDataRef gameData)
        : gameData_(gameData)
    {
        add_ = false;
        remove_ = false;
    }

    SceneManager::~SceneManager()
    {
    }

    void SceneManager::addScene(
        SceneRef scene, const bool replace, const bool replaceAll)
    {
        add_ = true;
        replace_ = replace;
        replaceAll_ = replaceAll;
        scene_ = std::move(scene);
    }

    void SceneManager::removeScene()
    {
        remove_ = true;
    }

    void SceneManager::process()
    {
        if (remove_)
        {
            scenes_.pop();

            remove_ = false;
            Logger::log("Scenes count: " + std::to_string(getScenesCount()));
        }
        else if (add_)
        {   
            if (replace_)
            {
                if (!scenes_.empty())
                {
                    do
                    {
                        scenes_.pop();
                    }
                    while(!scenes_.empty() && replaceAll_);
                }
            }
            
            if (!scenes_.empty())
            {
                scene_->setPreviousScene(getActiveScene().get());
            }

            scenes_.push(std::move(scene_));
            add_ = false;

            Logger::log("Scenes count: " + std::to_string(getScenesCount()));
        }
        if (scenes_.empty())
        {
            gameData_->gameRunning = false;
        }
    }

    const int SceneManager::getScenesCount()
    {
        return scenes_.size();
    }

    SceneRef& SceneManager::getActiveScene()
    {
        return scenes_.top();
    }
}