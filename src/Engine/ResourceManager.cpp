#include "Engine/ResourceManager.hpp"
#include "Engine/Logger.hpp"

#include <SDL2/SDL_image.h>

namespace Engine
{
    ResourceManager::ResourceManager(GameDataRef gameData)
        : gameData(gameData)
    {
        Engine::Logger::log("SDL IMG init");
        if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
        {
            Logger::showErrorBox("SDL IMG init failed: " + std::string(IMG_GetError()));
            throw;
        }
    }

    void ResourceManager::loadPNG(
        const std::string &path, 
        const std::string &resourceName)
    {
        SDL_Texture *newTexture = nullptr;

        SDL_Surface *loadedImage = IMG_Load(path.c_str());

        if (loadedImage == nullptr)
        {
            Logger::showErrorBox("SDL PNG load failed: " + std::string(IMG_GetError()));
            throw;
        }
        else
        {
            newTexture = SDL_CreateTextureFromSurface(gameData->renderer, loadedImage);
            if (newTexture == nullptr)
            {
                Logger::showErrorBox("SDL create texture failed: " + std::string(SDL_GetError()));
                throw;
            }
            SDL_FreeSurface(loadedImage);

            images.insert(std::make_pair(resourceName, newTexture));
        }
    }

    SDL_Texture* ResourceManager::getTexture(
        const std::string &resourceName)
    {
        return images[resourceName];
    }
}